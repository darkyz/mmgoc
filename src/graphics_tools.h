#ifndef _GRAPHTOOLS
#define _GRAPHTOOLS

#include "headers.h"

void loading(void); // affichent des icones "loading", "saved" ou "error". Le loading n'a pas de Sleep.
void saved(void);
void error(void);

#endif