#include "communication_tools.h"

int Tx_infos(void *player) // fonction de transmission des infos du perso au serveur
{
	DataPlayer buffer;
	Player2DataPlayer(*player, &buffer); // on convertit les donnees en DataPlayer
	return Serial_WriteBytes(&buffer, sizeof(DataPlayer));
}

int Rx_infos(void *pPlayers, unsigned char nbPlayers) // fonction de reception des infos du serveurs sur les différents persos. Cette fonction doit s'adapter au nombre de persos à afficher.
{
	short size = 0;
	unsigned char nbInfosGot;
	int status, i, j;
	DataPlayer *buffer = NULL;
	Player *players = NULL;
	
	players = pPlayers; // pour avoir acces aux structures
	
	Serial_ReadByte(&nbInfosGot); // on recupere le nombre de donnees a recuperer
	
	buffer = malloc(nbInfosGot * sizeof(DataPlayer); // on alloue une zone pour la reception des donnees
	
	status = Serial_ReadBytes(buffer, nbInfosGot * sizeof(DataPlayer), *size); // size contient le nombre d'octets lus
	
	if(status) return status; // si il s'est passe un probleme, on quitte la fonction
	
	for(i=0; i<nbInfosGot; ++i) // pour chaque joueur recu
	{
		for(j=0; j<nbPlayers; ++j) // on regarde pour chaque joueur en memoire
		{
			if(buffer[i]->ID == players[j]->ID) DataPlayer2Player(&(dataPlayer[i]), &(players[i])); // on met a jour les donnees
		}
	}
	
	free(buffer); // on libere la zone memoire
	return 0; // si tout s'est bien passe on retourne 0
}

int initGameFromServer(void *thisPlayer, void *otherPlayers, unsigned char *nbPlayersOnLine)
/* Valeurs de retour :

0 : communication etablie, donnees recuperees.
1 : communication annulee (delai depasse)
2 : communication annulee (annulation manuelle)
3 : allocation memoire annulee 

*/
{
	int time, timeInit, size;
	unsigned char data;
	DataPlayer buffer;
	
	Serial_ClearTransmitBuffer(); // on libere tout
	Serial_ClearReceiveBuffer();
	
	timeInit = RTC_getTicks();
	
	loading(); // on affiche le message d'attente
	
	while(!Serial_IsOpen()) // On teste la communication avec le serveur
	{
		time = RTC_getTicks();
		if(time > timeInit + 128 * 5) return 1 // si le temps est écoulé, on retourne 0 (5 secondes)
		if(IsKeyDown(KEY_CTRL_AVEC)) return 2; // si on annule manuellement, on retourne -1
	}
	
	Serial_WriteByte(1);
	Serial_WriteBytes(thisPlayer, sizeof(Player)); // envoie les donnees au serveur (pseudo essentiellement)
	
	while(Serial_ReadBytes(buffer, sizeof(DataPlayer), &size); // on met a jour notre joueur
	DataPlayer2Player(&buffer, thisPlayer);
	
	Serial_WriteByte(2); // on lance une requete pour recuperer le nombre de joueurs
	
	while(Serial_ReadByte(nbPlayersOnLine)) // on recupere le nombre de joueurs a initialiser
	
	otherPlayers = malloc(*nbPlayersOnLine * sizeof(Player)); // on cree une zone de taille suffisante
	
	Serial_WriteByte(3); // on lance une requete pour recuperer les infos des autres joueurs
	
	while(Serial_ReadBytes(otherPlayers, nbPlayersToInit * sizeof(Player), &size); // on ecrit les infos des autres joueurs dans la zone dediee 
	
	Serial_WriteByte(4); // on lance une requete pour enclencher la partie cote serveur
	
	while(data != 5) Serial_ReadByte(&data); // on attend la confirmation du serveur
	
	return 0; // si tout s'est bien passe, on lance la partie
}

void quitGame(void)
{
	Serial_close(1);
}

void DataPlayer2Player(void *dataPlayer, void *player)
{
	DataPlayer *pDP = NULL;
	Player *P = NULL;
	
	pDP = dataPlayer;
	P = player;
	
	P->posX = pDP->posX;
	P->posY = pDP->posY;
	P->action = pDP->action;
	P->vie = pDP->vie;
}

void Player2DataPlayer(void *player, void *dataPlayer)
{
	DataPlayer *pDP = NULL;
	Player *P = NULL;
	
	pDP = dataPlayer;
	P = player;
	
	pDP->ID = P->ID
	pDP->posX = P->posX;
	pDP->posY = P->posY;
	pDP->action = P->action;
	pDP->vie = P->vie;
}
