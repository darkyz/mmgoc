#include "game.h"

void game(void)
{
	/*** INITIALISATION ***/
	
	Player thisPlayer = {0, ".Pseudo.", 0, 0, 0, 100};
	Player *otherPlayers = NULL;
	unsigned char nbPlayersOnLine;
	int error, i, j;
	int time = 0;
	
	unsigned char conf[] = {0,BAUD,0,0,0,0}; // ouverture des ports de communication
	Serial_Open(conf);
	
	error = initGameFromServer(&thisPlayer, otherPlayers, *nbPlayersOnLine);
	
	switch(error)
	{
		case 0 : break; // si pas d'erreur, on passe a la suite
		case 1 : error(); return; // voir initGameFromServor()
		case 2 : error(); return;
		case 3 : error(); return;
		
		default : break;
	}
	
	/*** BOUCLE PRNCIPALE ***/
	
	while(1)
	{
		/** Input **/
		if(IsKeyDown(KEY_CTRL_LEFT)) --thisPlayer.posX;
		if(IsKeyDown(KEY_CTRL_RIGHT) ++thisPlayer.posX;
		if(IsKeyDown(KEY_CTRL_UP)) --thisPlayer.posY;
		if(IsKeyDown(KEY_CTRL_DOWN)) ++thisPlayer.posY;
		
		if(IsKeyDown(KEY_CRTL_SHIFT)) thisPlayer.action = 1;
		
		if(thisPLayer.posX < 0) player.posX = 0;
		if(thisPLayer.posX > 65535) player.posX = 65535;
		if(thisPLayer.posY < 0) player.posY = 0;
		if(thisPLayer.posY > 65335) player.posY = 65535;
		
		/** Echange donnees avec le serveur **/
		Tx_infos(&thisPlayer);
		Rx_infos(players, nbPlayersOnLine);
		
		/** Affichage **/
		ML_clear_vram();
		
		ML_point(64, 32, 3); // affichage utilisateur au centre
		
		for(i=0; i<nbPlayersOnLine; ++i)
		{
			if(otherPlayers[i]->ID)
			{
				ML_point(otherPlayers[i]->posX - thisPlayer.posX + 64, otherPlayers[i]->posY - thisPlayer.posX + 32, 3); // affichage des autres joueurs
				PrintMini(otherPlayers[i]->posX - thisPlayer.posX + 48, otherPlayers[i]->posY - thisPlayer.posX + 22, otherPlayers[i].pseudo, 1);
			}
		}
		
		ML_rectangle(64 - thisPlayer.posX, 32 - thisPlayer.posY, 65599 - thisPlayer.posX, 65567 - thisPlayer.posY, 1, ML_BLACK, ML_TRANSPARENT); // bordures de la carte
		
		ML_display_vram();
	}
}