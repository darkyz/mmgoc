#ifndef _COMTOOLS
#define _COMTOOLS

#include "headers.h"

int Tx_infos(void *player); // transmission des infos au serveur
int Rx_infos(void *infos, unsigned char nbPlayers); // recuperation des infos du serveur

int initGameFromServer(void *thisPlayer, void *otherPlayers, unsigned char *nbPlayersOnLine); // initialisation des infos depuis le serveur
void quitGame(void);

void DataPlayer2Player(void *dataPlayer, void *player); // met a jour les donnes d'un joueur depuis un DataPlayer.
void Player2DataPlayer(void *player, void *dataPlayer); // selectionne les donnees a envoyer dans un DataPlayer

#endif