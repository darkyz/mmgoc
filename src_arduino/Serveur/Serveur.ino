#include <SoftwareSerial.h>

#define BAUDS 9600 // vitesse de transmission
#define nbCalcs 5 // nombre max de caltos reliees à l'Arduino

SoftwareSerial calc1(2, 3);
SoftwareSerial calc2(4, 5);
SoftwareSerial calc3(6, 7);
SoftwareSerial calc4(8, 9);
SoftwareSerial calc5(10, 11);

SoftwareSerial calcs[nbCalcs] = { calc1, calc2, calc3, calc4, calc5 };

/*** DECLARATION DES VARIABLES ET DES STRUCTURES ***/
typedef struct
{
	unsigned char ID;
	unsigned char pseudo[9]; // max 9-1 = 8 caracteres (1 bit d'arret)
	// unsigned char avatar[32];
	unsigned short posX;
	unsigned short posY;
	unsigned char action;
	char vie;
} Player;

/* Infos a transmettre a chaque frame */
typedef struct 
{
	unsigned char ID;
	unsigned short posX;
	unsigned short posY;
	unsigned char action;
	char vie;
} DataPlayer;

Player players[nbCalcs];
char status_com[nbCalcs];
int i, j;

/*** DECLARATION DES FONCTIONS ***/

void DataPlayer2Player(char *buffer, int player)
{	
  DataPlayer pDP;
  memcpy(&pDP, buffer, sizeof(DataPlayer));
  players[player].posX = pDP.posX;
  players[player].posY = pDP.posY;
  players[player].action = pDP.action;
  players[player].vie = pDP.vie;
}

void Player2DataPlayer(unsigned char *buffer, int player)
{
  DataPlayer pDP;
  pDP.ID = players[player].ID;
  pDP.posX = players[player].posX;
  pDP.posY = players[player].posY;
  pDP.action = players[player].action;
  pDP.vie = players[player].vie;
  memcpy(buffer, &pDP, sizeof(DataPlayer));
}

void refreshFromPlayer(int port)
{
  char buffer[sizeof(DataPlayer)];
  calcs[port].readBytes(buffer, sizeof(DataPlayer));
  DataPlayer2Player(buffer, port);
}

void refreshToPlayer(int port)
{
  unsigned char buffer[sizeof(Player)];
  calcs[port].write(nbPlayersOnLine);
  for(j=0; j<nbCalcs; ++j)
  {
    if(port-j && players[j].ID)
    {
      Player2DataPlayer(buffer, j);
      memcpy(buffer, &players[i], sizeof(DataPlayer));
      calcs[port].write(buffer, sizeof(DataPlayer));
    }
  }
}

void sendInitGame(int port)
{
  unsigned char buffer[sizeof(Player)];
  for(j=0; j<nbCalcs; ++j)
  {
    memcpy(buffer, &players[port], sizeof(Player));
    if(i-j) calcs[port].write(buffer, sizeof(Player));
  }
}

void getInfosInitPlayer(int port)
{
  char buffer[sizeof(Player)];
  calcs[port].readBytes(buffer, sizeof(Player));
  memcpy(&players[port], buffer, sizeof(Player));
}

void sendInfoInitPlayer(int port)
{
  unsigned char buffer[sizeof(Player)];
  memcpy(buffer, &players[port], sizeof(Player));
  calcs[port].write(buffer, sizeof(Player));
}

/*** DEBUT DU CODE PAR DEFAUT ***/
void setup()
{  
  // Ouvre les ports serials
  Serial.begin(BAUDS); //Pour la com avec l'ordi. Dont les ports sont Tx:1, Rx:0
  
  // ouvre les ports avec les caltos
  for(i=0; i<nbCalcs; ++i)
  {
    calcs[i].begin(BAUDS); // on ouvre les ports
    players[i].ID = 0; // on initialise les ID
    players[i].posX = 100; // la position en X
    players[i].posY = 100; // la position en Y
    players[i].action = 0; // les actons en cours
    status_com[i] = 0; // le status de la communication
  }
}

void loop()
{
  /*** MISE A JOUR DES DONNEES ***/
  for(i=0; i<nbCalcs; ++i)
  {
    switch(status_com[i])
    {
      case 0: if(calcs[i].read() == 1){
        status_com[i] = 1; // voir le wiki pour les differentes etapes d'initialisation
        getInfosInitPlayer(i);
        players[i].ID = i;
        sendInfoInitPlayer(i);
        }
        break;
      
      case 1: if(calcs[i].read() == 2){
        status_com[i] = 2;
        calcs[i].write(nbCalcs - 1);
        }
        break;
      
      case 2: if(calcs[i].read() == 3){
        status_com[i] = 3;
        sendInitGame(i);
        }
        break;
        
      case 3: if(calcs[i].read() == 4){
        status_com[i] = 4;
        calcs[i].write(5);
        }
        break;
        
      case 4: refreshFromPlayer(i);
        refreshToPlayer(i);
        ++nbPlayersOnLine;
        break;
    }
  }
  
  /*** TRAITEMENT DES DONNEES ***/
  
  
}

