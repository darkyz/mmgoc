#ifndef _GAME
#define _GAME

#include "headers.h"

#define BAUD 5 // vitesse de communication : 5 = 9600 bauds. Voir syscall.h

void game(void);

/*** Infos globales du joueur (comprend le pseudo, et toutes les donnees d'initialisation) ***/
typedef struct
{
	unsigned char ID;
	unsigned char pseudo[9]; // max 9-1 = 8 caracteres (1 bit d'arret)
	// unsigned char avatar[32];
	unsigned short posX;
	unsigned short posY;
	unsigned char action;
	char vie;
} Player;

/*** Infos a transmettre a chaque frame ***/
typedef struct 
{
	unsigned char ID;
	unsigned short posX;
	unsigned short posY;
	unsigned char action;
	char vie;
} DataPlayer;

#endif