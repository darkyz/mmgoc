// Pas de define pour inclure ce fichier dans tous les headers concernes.
// Ajouter ici tous les headers du projet. Permet de tout mettre a jour automatiquement.

#include "fxlib.h"
#include "MonochromeLib.h"
#include "time.h"
#include "syscall.h"
#include "communication_tools.h"
#include "graphics_tools.h"
#include "string.h"
#include "game.h"
#include "stdlib.h"
#include "tools.h"