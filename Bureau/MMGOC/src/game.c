#include "game.h"

void game(void)
{
	/*** INITIALISATION ***/
	
	Player thisPlayer = {0, ".Pseudo.", 0, 0, 0, 100};
	Player *otherPlayers = NULL;
	unsigned char nbPlayersOnLine;
	int errorType, i, j;
	int time = 0;
	
	unsigned char conf[] = {0,BAUD,0,0,0,0}; // ouverture des ports de communication
	Serial_Open(conf);
	
	errorType = initGameFromServer(&thisPlayer, otherPlayers, &nbPlayersOnLine);
	
	switch(errorType)
	{
		case 0 : break; // si pas d'erreur, on passe a la suite
		case 1 : error(); return; // voir initGameFromServor()
		case 2 : error(); return;
		case 3 : error(); return;
		
		default : break;
	}
	
	/*** BOUCLE PRNCIPALE ***/
	
	while(1)
	{
		/** Input **/
		if(IsKeyDown(KEY_CTRL_LEFT)) --thisPlayer.posX;
		if(IsKeyDown(KEY_CTRL_RIGHT)) ++thisPlayer.posX;
		if(IsKeyDown(KEY_CTRL_UP)) --thisPlayer.posY;
		if(IsKeyDown(KEY_CTRL_DOWN)) ++thisPlayer.posY;
		
		if(IsKeyDown(KEY_CTRL_SHIFT)) thisPlayer.action = 1;
		
		if(thisPlayer.posX < 0) thisPlayer.posX = 0;
		if(thisPlayer.posX > 65535) thisPlayer.posX = 65535;
		if(thisPlayer.posY < 0) thisPlayer.posY = 0;
		if(thisPlayer.posY > 65335) thisPlayer.posY = 65535;
		
		/** Echange donnees avec le serveur **/
		Tx_infos(&thisPlayer);
		Rx_infos(otherPlayers, nbPlayersOnLine);
		
		/** Affichage **/
		ML_clear_vram();
		
		ML_point(64, 32, 3, ML_BLACK); // affichage utilisateur au centre
		
		for(i=0; i<nbPlayersOnLine; ++i)
		{
			if(otherPlayers[i].ID)
			{
				ML_point(otherPlayers[i].posX - thisPlayer.posX + 64, otherPlayers[i].posY - thisPlayer.posX + 32, 3, ML_BLACK); // affichage des autres joueurs
				PrintMini(otherPlayers[i].posX - thisPlayer.posX + 48, otherPlayers[i].posY - thisPlayer.posX + 22, otherPlayers[i].pseudo, 1);
			}
		}
		
		ML_rectangle(64 - thisPlayer.posX, 32 - thisPlayer.posY, 65599 - thisPlayer.posX, 65567 - thisPlayer.posY, 1, ML_BLACK, ML_TRANSPARENT); // bordures de la carte
		
		ML_display_vram();
	}
}